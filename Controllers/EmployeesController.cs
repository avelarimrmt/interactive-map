﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using OfficeMap.Models;

namespace OfficeMap.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly OfficeMapDbContext _db;

        public EmployeesController(OfficeMapDbContext context)
        {
            _db = context;
        }
        
        [HttpGet("starts-with/{startOfName}")]
        public IEnumerable<Employee> GetEmployeesBySubstring(string startOfName)
        {
            var employees = _db.Employees
                .Where(emp =>
                    emp.LastName.ToLower().StartsWith(startOfName.ToLower()) ||
                    emp.FirstName.ToLower().StartsWith(startOfName.ToLower()));

            return employees
                .Include(emp => emp.Position)
                .Include(emp => emp.Desk)
                .Include(emp => emp.Photo)
                .Include(emp => emp.Status)
                .ToList();
        }
        
        [HttpGet("by-em-id/{empId}")]
        public Employee GetEmployeeById(int empId)
        {
            return _db.Employees
                .Where(emp => emp.Id == empId)
                .Include(emp => emp.Position)
                .Include(emp => emp.Desk)
                .Include(emp => emp.Photo)
                .Include(emp => emp.Status).First();
        }
        
        [HttpGet("by-id/{deskId}")]
        public IEnumerable<Employee> GetEmployeeByDeskId(int deskId)
        {
            return _db.Employees
                .Where(emp => emp.DeskId == deskId)
                .Include(emp => emp.Desk)
                .Include(emp => emp.Position)
                .Include(emp => emp.Photo)
                .Include(emp => emp.Status)
                .ToList();
        }
        
        [HttpGet("by-employees")]
        public IEnumerable<Employee> GetEmployees()
        {
            return _db.Employees
                .Include(emp => emp.Position)
                .Include(emp => emp.Desk)
                .Include(emp => emp.Photo)
                .Include(emp => emp.Status)
                .ToList();
        }

        [HttpPatch("by-emp-id/{empId}")]
        public Employee PatchEmployeeById(int empId)
        {
            var employee = _db.Employees
                .Include(emp => emp.Position)
                .Include(emp => emp.Desk)
                .Include(emp => emp.Photo)
                .Include(emp => emp.Status)
                .First(emp => emp.Id == empId);
            
            foreach(var pair in Request.Form) {
                employee.GetType().InvokeMember(pair.Key,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
                    Type.DefaultBinder, employee, pair.Value);
            }

            _db.SaveChanges();
            
            return employee;
        }
        
        [HttpPost("add-emp")]
        public Employee AddEmployee()
        {
            var employee = new Employee();
            
            foreach(var pair in Request.Form) {
                employee.GetType().InvokeMember(pair.Key,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty,
                    Type.DefaultBinder, employee, pair.Value);
            }
            _db.Add(employee);
            _db.SaveChanges();
            
            return employee;
        }
        
        [HttpPost("delete-emp/{empId}")]
        public Employee DeleteEmployee(int empId)
        {
            var employee = _db.Employees
                .Include(emp => emp.Position)
                .Include(emp => emp.Desk)
                .Include(emp => emp.Photo)
                .Include(emp => emp.Status)
                .First(emp => emp.Id == empId);
            
            _db.Remove(employee);
            _db.SaveChanges();
            
            return employee;
        }
    }
}