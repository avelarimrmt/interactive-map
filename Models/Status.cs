﻿namespace OfficeMap.Models
{
    public partial class Status
    {
        public Status()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}